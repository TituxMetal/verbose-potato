<?php

require 'vendor/autoload.php';

$cat = new \App\Cat\Cat(
	new \App\Cat\Action\Speak\MeowAction,
	new \App\Cat\Action\Jump\JumpAction
);

$cat->setSpeakAction(new \App\Cat\Action\Speak\GrowlAction);

echo $cat->speak();