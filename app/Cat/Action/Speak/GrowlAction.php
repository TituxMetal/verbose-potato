<?php

namespace App\Cat\Action\Speak;

use App\Cat\Contract\SpeakActionInterface;

class GrowlAction implements SpeakActionInterface {

	public function speak() {
		return 'Growwwwwl!';
	}

}