<?php

namespace App\Cat\Action\Speak;

use App\Cat\Contract\SpeakActionInterface;

class MeowAction implements SpeakActionInterface {

	public function speak() {
		return 'Meow';
	}

}