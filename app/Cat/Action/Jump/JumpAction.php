<?php

namespace App\Cat\Action\Jump;

use App\Cat\Contract\JumpActionInterface;

class JumpAction implements JumpActionInterface {

	public function jump() {
		return 'Jump!';
	}

}