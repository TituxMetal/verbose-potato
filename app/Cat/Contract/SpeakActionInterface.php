<?php

namespace App\Cat\Contract;

interface SpeakActionInterface {

	public function speak();

}