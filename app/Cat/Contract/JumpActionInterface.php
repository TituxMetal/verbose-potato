<?php

namespace App\Cat\Contract;

interface JumpActionInterface {

	public function jump();

}